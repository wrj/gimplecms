#Read Me, please

__Gimple CMS__, is a small cms (content management system) made with Grails v3 for internal usage.

I've found that it may be good enough to other people who want to create a new website with
Grails. S, I decided to share it to community.

We, at ___Skoode Skill Co.,Ltd.___ _(Bangkok, TH)_ want you guys to suggest what you may want or
even to
contribute to the project.

Contact me directly for further information,

__Worajedt S.__<br>[sjedt@skoodeskill.com](!mailto:sjedt@skoodeskill.com)

__Contributors__<br>
Piyawat D.<br>
Passakorn R.<br>

##How to config

1.  แก้ไข username password ของ database postgresql ที่ไฟล์ application.yml
2.  สร้าง database ชื่อ gimplecms
3.  config path สำหรับ project ที่ไฟล์ application.yml ที่หัวข้อ
    pathUpload:
    pathProject:
4.  url ที่ใช้ตอนนี้มี 2 url
    /news
    /banner