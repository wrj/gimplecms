<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
    <meta name="twitter:widgets:theme" content="light">
    <meta property="og:title" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://www.melaniebangkok.com/images/slide3.jpg"/>
    <meta property="og:description" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <title>Melanie Bangkok ถนนจันทร์-สาทร</title>
    <link rel="shortcut icon" type="image/x-icon" href="/assets/front/theme-mountain-favicon.ico">



    <!-- Css -->
    <g:if test="${session.lang == "Th"}">
        <asset:stylesheet src="front/thai-fonts.css"/>
    </g:if>
    <g:if test="${session.lang == "En"} ${session.lang == "Ch"}">
        <asset:stylesheet src="front/eng-fonts.css"/>
    </g:if>
    <asset:stylesheet src="front/core.min.css"/>
    <asset:stylesheet src="front/skin-architecture.css"/>
    <asset:stylesheet src="front/header.css"/>
    <asset:stylesheet src="front/index.css"/>
    <asset:stylesheet src="front/gallery.css"/>
    <asset:stylesheet src="front/footer.css"/>

</head>

<body class="shop home-page">

<g:render template="headerCollapse"/>

<div class="wrapper reveal-side-navigation">
    <div class="wrapper-inner">

        <!-- Header -->
        <header class="header header-fixed header-top header-fixed-on-mobile header-transparent" data-sticky-threshold="window-height" data-bkg-threshold="100">
            <div class="header-inner">
                <div class="row nav-bar clear-row-mg">
                    <div class="column width-12 nav-bar-inner">
                        <div class="logo">
                            <div class="logo-inner">
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                            </div>
                        </div>
                        <div>
                            <nav class="navigation nav-block secondary-navigation nav-right">
                                <ul>
                                    <li class="aux-navigation hide">
                                        <!-- Aux Navigation -->
                                        <a href="#" class="navigation-show side-nav-show nav-icon">
                                            <span class="icon-menu"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right language-key">
                            <a href="<g:createLink action="galleryList" params="[lang:'Th']" id="${gallery.getId()}"/>"><strong>TH</strong></a> |
                            <a href="<g:createLink action="galleryList" params="[lang:'En']" id="${gallery.getId()}"/>"><strong>EN</strong></a>
                            | <a href="<g:createLink action="galleryList" params="[lang:'zh_Cn']" id="${gallery.getId()}"/>"><strong>中文</strong></a>
                        </div>
                        <nav class="navigation nav-block primary-navigation nav-right no-margin-right link-hilight">
                            <g:render template="header"/>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header End -->


        <!-- Content -->
        <div class="content clearfix">

            <!-- Portfolio Grid -->
            <div class="section-block grid-container fade-in-progressively full-width no-padding no-margins bg-pic"
                 data-layout-mode="masonry" data-grid-ratio="1.5" data-animate-filter-duration="700" data-set-dimensions
                 data-animate-resize data-animate-resize-duration="0">
                <div class="row">
                    <div class="column width-12 news-topic">
                        <h2>${gallery["title${lang}"]}</h2>
                        <img src="/assets/front/line.gif">
                    </div>
                    <div class="mg-pic">
                        <div class="column width-12 no-padding">
                            <div class="row grid content-grid-3 clear-pd-mobile">
                                <g:each in="${galleryImage}">
                                    <div class="grid-item grid-sizer">
                                        <div class="thumbnail img-scale-in" data-hover-easing="easeInOut"
                                             data-hover-speed="500" data-hover-bkg-color="#ffffff"
                                             data-hover-bkg-opacity="1">
                                            <a class="overlay-link lightbox-link" data-toolbar="zoom"
                                               data-group="gallery-1" data-caption="Melanie Bangkok - Chan-Sathon"
                                               href="<g:resource dir="uploadresource/gallery/"
                                                                 file="${it["imageName"]}"/>">
                                                <img src="<g:resource dir="uploadresource/gallery/"
                                                                      file="${it["imageName"]}"/>" alt=""/>
                                                %{--<span class="overlay-info">--}%
                                                    %{--<span>--}%
                                                    %{--<span>--}%
                                                    %{--<span class="project-title">${it["title${lang}"]}</span>--}%
                                                    %{--<span class="project-description">${it["detail${lang}"]}</span>--}%
                                                    %{--</span>--}%
                                                    %{--</span>--}%
                                                %{--</span>--}%
                                            </a>
                                        </div>
                                    </div>
                                </g:each>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Portfolio Grid End -->

        </div>
        <!-- Content End -->

        <g:render template="footer"/>

    </div>
</div>

<!-- Js -->
<asset:javascript src="front/jquery-3.1.1.min.js"/>
<asset:javascript src="front/timber.master.min.js"/>
</body>
</html>