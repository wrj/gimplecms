<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
    <meta name="twitter:widgets:theme" content="light">
    <meta property="og:title" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://www.melaniebangkok.com/images/slide3.jpg"/>
    <meta property="og:description" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <title>Melanie Bangkok ถนนจันทร์-สาทร</title>
    <link rel="shortcut icon" type="image/x-icon" href="/assets/front/theme-mountain-favicon.ico">



    <!-- Css -->
    <g:if test="${session.lang == "Th"}">
        <asset:stylesheet src="front/thai-fonts.css"/>
    </g:if>
    <g:if test="${session.lang == "En"} ${session.lang == "Ch"}">
        <asset:stylesheet src="front/eng-fonts.css"/>
    </g:if>
    <asset:stylesheet src="front/core.min.css"/>
    <asset:stylesheet src="front/skin-architecture.css"/>
    <asset:stylesheet src="front/header.css"/>
    <asset:stylesheet src="front/index.css"/>
    <asset:stylesheet src="front/project.css"/>
    <asset:stylesheet src="front/footer.css"/>
    <asset:stylesheet src="jquery.bxslider/jquery.bxslider.css"/>

    %{--<link href="/assets/javascript/jquery.bxslider/jquery.bxslider.css" rel="stylesheet"/>--}%

</head>

<body class="shop home-page">

<g:render template="headerCollapse"/>

<div class="wrapper reveal-side-navigation">
    <div class="wrapper-inner">

        <!-- Header -->
        <header class="header header-fixed header-top header-fixed-on-mobile header-transparent" data-sticky-threshold="window-height" data-bkg-threshold="100">
            <div class="header-inner">
                <div class="row nav-bar clear-row-mg">
                    <div class="column width-12 nav-bar-inner">
                        <div class="logo">
                            <div class="logo-inner">
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                            </div>
                        </div>
                        <div>
                            <nav class="navigation nav-block secondary-navigation nav-right">
                                <ul>
                                    <li class="aux-navigation hide">
                                        <!-- Aux Navigation -->
                                        <a href="#" class="navigation-show side-nav-show nav-icon">
                                            <span class="icon-menu"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right language-key">
                            <a href="<g:createLink action="project" params="[lang:'Th']"/>"><strong>TH</strong></a> |
                            <a href="<g:createLink action="project" params="[lang:'En']"/>"><strong>EN</strong></a>
                            | <a href="<g:createLink action="project" params="[lang:'zh_Cn']"/>"><strong>中文</strong></a>
                        </div>
                        <nav class="navigation nav-block primary-navigation nav-right no-margin-right link-hilight">
                            <g:render template="header"/>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header End -->


        <!-- Content -->
        <div class="content clearfix">

        <!--Banner-->
        <div class="project-banner">
            <img src="/assets/front/projectbg.jpg" class="hide-on-mobile show-on-mobile3">
            <img src="/assets/front/projectbg@2x.jpg" class="hide show-on-mobile">
            <div class="project">
                <div class="project-menu">
                    <p><g:message code="project.topic1"/> <br/><g:message code="project.topic2"/></p>
                    <ul>
                        <li><a href="javascript:;" id="slide-to1"><g:message code="project.slide.concept"/></a></li>
                        <li><a href="javascript:;" id="slide-to2"><g:message code="project.slide.exclusivity"/></a></li>
                        <li><a href="javascript:;" id="slide-to3"><g:message code="project.slide.design"/></a></li>
                        <li><a href="javascript:;" id="slide-to4"><g:message code="project.slide.transportation"/></a></li>
                        <li><a href="javascript:;" id="slide-to5"><g:message code="project.slide.facility"/></a></li>
                        <li><a href="javascript:;" id="slide-to6"><g:message code="project.slide.location"/></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--Banner End-->


        <!--slide-->

        <div class="section-block no-padding">
            <ul class="bxslider">
                <!--concept-->
                <li>
                    <div class="row no-margins">
                        <div class="column width-7" align="center">
                            <div class="hero-content-inner horizon project-slide">
                                <h2 class="mb-30"><g:message code="project.slide1.topic"/></h2>
                                <hr/>
                                <g:if test="${concept}">
                                    ${raw(concept["detail${lang}"])}
                                </g:if>
                            </div>
                        </div>
                        <a class="lightbox-link column width-5" data-toolbar="zoom"
                           data-group="gallery-1" href="/assets/front/project/concept.jpg">
                            <img src="/assets/front/project/concept.jpg" class="img-width">
                        </a>
                    </div>
                </li>

                <!--exclusivity-->
                <li>
                    <div class="row no-margins">
                        <div class="column width-7" align="center">
                            <div class="hero-content-inner horizon project-slide">
                                <h2 class="mb-30"><g:message code="project.slide2.topic"/></h2>
                                <hr/>
                                <g:if test="${exclusivity}">
                                    ${raw(exclusivity["detail${lang}"])}
                                </g:if>
                            </div>
                        </div>
                        <a class="lightbox-link column width-5" data-toolbar="zoom"
                           data-group="gallery-1" href="/assets/front/project/exclusivity.jpg">
                            <img src="/assets/front/project/exclusivity.jpg" class="img-width">
                        </a>
                    </div>
                </li>

                <!--design-->
                <li>
                    <div class="row no-margins">
                        <div class="column width-7" align="center">
                            <div class="hero-content-inner horizon project-slide">
                                <h2 class="mb-30"><g:message code="project.slide3.topic"/></h2>
                                <hr/>
                                <g:if test="${design}">
                                    ${raw(design["detail${lang}"])}
                                </g:if>
                            </div>
                        </div>
                        <a class="lightbox-link column width-5" data-toolbar="zoom"
                           data-group="gallery-1" href="/assets/front/project/design.jpg">
                            <img src="/assets/front/project/design.jpg" class="img-width">
                        </a>
                    </div>
                </li>


                <!--transportation-->
                <li>
                    <div class="row no-margins">
                        <div class="column width-7" align="center">
                            <div class="hero-content-inner horizon project-slide">
                                <h2 class="mb-30"><g:message code="project.slide4.topic"/></h2>
                                <hr/>
                                <g:if test="${transportation}">
                                    ${raw(transportation["detail${lang}"])}
                                </g:if>
                            </div>
                        </div>
                        <a class="lightbox-link column width-5" data-toolbar="zoom"
                           data-group="gallery-1" href="/assets/front/transport.jpg">
                            <img src="/assets/front/project/transport.jpg" class="img-width">
                        </a>
                    </div>
                </li>

                <!--facility-->
                <li>
                    <div class="row no-margins">
                        <div class="column width-7" align="center">
                            <div class="hero-content-inner horizon project-slide">
                                <h2 class="mb-30"><g:message code="project.slide5.topic"/></h2>
                                <hr/>
                                <g:if test="${facility}">
                                    ${raw(facility["detail${lang}"])}
                                </g:if>
                            </div>
                        </div>
                        <a class="lightbox-link column width-5" data-toolbar="zoom"
                           data-group="gallery-1" href="/assets/front/project/facility.jpg">
                            <img src="/assets/front/project/facility.jpg" class="img-width">
                        </a>
                    </div>
                </li>


                <!--location-->
                <li>
                    <div class="row no-margins">
                        <div class="column width-7 ">
                            <div class="hero-content-inner horizon project-slide">
                                <h2 class="mb-30"><g:message code="project.slide6.topic"/></h2>
                                <hr/>
                                <g:if test="${location}">
                                    ${raw(location["detail${lang}"])}
                                </g:if>
                            </div>
                        </div>
                        <a class="lightbox-link column width-5" data-toolbar="zoom"
                           data-group="gallery-1" href="/assets/front/project/location.jpg">
                            <img src="/assets/front/project/location.jpg" class="img-width">
                        </a>
                    </div>
                </li>
            </ul>
        </div>
        <!--slide End-->

        </div>
        <!-- Content End -->

        <g:render template="footer"/>

    </div>
</div>

<!-- Js -->
<asset:javascript src="front/jquery-1.12.4.min.js"/>
%{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--}%
<asset:javascript src="front/timber.master.min.js"/>
<asset:javascript src="jquery.bxslider/jquery.bxslider.min.js"/>
<script type="text/javascript">

    $(document).ready(function () {

        var slider = $('.bxslider').bxSlider({
            controls: false
        });

        $('#slide-to6').on('click', function () {

            slider.goToSlide(5);

        });
        $('#slide-to5').on('click', function () {

            slider.goToSlide(4);

        });
        $('#slide-to4').on('click', function () {

            slider.goToSlide(3);

        });
        $('#slide-to3').on('click', function () {

            slider.goToSlide(2);

        });
        $('#slide-to2').on('click', function () {

            slider.goToSlide(1);

        });
        $('#slide-to1').on('click', function () {

            slider.goToSlide(0);

        });

    });
</script>
</body>
</html>