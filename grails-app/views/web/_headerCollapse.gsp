<!-- Side Navigation Menu -->
<aside class="side-navigation-wrapper enter-right" data-no-scrollbar data-animation="scale-in">
    <div class="side-navigation-scroll-pane">
        <div class="side-navigation-inner">
            <div class="side-navigation-header">
                <div class="navigation-hide side-nav-hide">
                    <a href="#">
                        <span class="icon-cancel medium"></span>
                    </a>
                </div>
            </div>

            <nav class="side-navigation">
                <ul>
                    <li><a href="<g:createLink action="index"/>"><g:message code="header.navbar.home"/></a></li>
                    <li><a href="<g:createLink action="register"/>"><g:message code="header.navbar.register"/></a></li>
                    <li><a href="<g:createLink action="project"/>"><g:message code="header.navbar.project"/></a></li>
                    <li><a href="<g:createLink action="plan"/>"><g:message code="header.navbar.unitPlan"/></a></li>
                    <li><a href="<g:createLink action="gallery"/>"><g:message code="header.navbar.gallery"/></a></li>
                    <li><a href="<g:createLink action="news"/>"><g:message code="header.navbar.newsProgress"/></a></li>
                    <li><a href="<g:createLink action="contact"/>"><g:message code="header.navbar.contact"/></a></li>
                    <li><a href="<g:createLink action="developer"/>"><g:message code="header.navbar.developer"/></a></li>
                </ul>

            </nav>
            <div class="side-navigation-footer">
                <ul class="social-list list-horizontal">
                    <li><a target="_blank" href="mailto:info@primegatesrealty.com"><span class="icon-mail small"></span></a>
                    </li>
                    <li><a target="_blank" href="https://www.facebook.com/melaniebangkok/"><span
                            class="icon-facebook small"></span></a></li>
                    <li><a target="_blank" href="https://www.instagram.com/melanie_bangkok/"><span
                            class="icon-instagram small"></span></a></li>
                </ul>
                <p class="copyright no-margin-bottom">&copy; 2016 Melanie Bangkok.</p>
            </div>
        </div>
    </div>
</aside>
<!-- Side Navigation Menu End -->