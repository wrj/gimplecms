<div class="form-group">
    <label for="inputTitleTh" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10">
        <input type="text" name="titleTh" class="form-control" id="inputTitleTh" placeholder="title" value="${gallery.getTitleTh()}" required>
    </div>
</div>
<div class="form-group">
    <label for="inputDetailTh" class="col-sm-2 control-label">Detail</label>
    <div class="col-sm-10">
        <textarea name="detailTh" class="form-control"
                  id="inputDetailTh">${gallery.getDetailTh() ?: ""}</textarea>
    </div>
</div>