<!--- File Upload --->
<div class="form-group">
    <label for="imageName" class="col-sm-2 control-label">File Upload</label>

    <div class="col-sm-10">
        <input type="text" value="" class="form-control" style="width:200px;float:left;margin-right:10px"
               readonly="readonly" name="imageName" id="imageName">
        <input type="button" value="Browse" onclick="showDialog()" class="btn btn-default"/>
        <p class="help-block">ขนาดรูปภาพ 1022 * 684 px</p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Thumbnail</label>

    <div class="col-sm-10">
        <ul class="thumbnails">

            <g:if test="${galleryImage.getImageName() && galleryImage.getImageName() != ""}">
                <li class="col-md-3">
                    <div class="thumbnail">
                        <a class="glyphicon glyphicon-zoom-in pull-right" href="javascript:previewimage('<g:resource dir="uploadresource/gallery/" file="${galleryImage.getImageName()}"/>')"></a>
                        <a href="javascript:previewimage('<g:resource dir="uploadresource/gallery/" file="${galleryImage.getImageName()}"/>')">
                            <img src="<g:resource dir="uploadresource/gallery/" file="${galleryImage.getImageName()}"/>" class="img-thumb img-rounded"/>
                        </a>
                        <p>Current Thumbnail</p>
                    </div>
                </li>
            </g:if>

            <li class="col-md-3" id="previewthumb">

            </li>
        </ul>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="<g:createLink action="editForm" id="${gallery.getId()}"/>" class="btn btn-default">Cancel</a>
    </div>
</div>
<!---    Preview Thumb--->
<div class="modal fade" role="dialog" id="pModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Preview Image</h3>
            </div>
            <div class="modal-body" id="previewimage">

            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </div>
</div>
<!---    Upload--->
<div class="modal fade" tabindex="-1" role="dialog" id="bModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload Image</h4>
            </div>

            <div class="modal-body">
                <div id="resultupload"></div>

                <div id="queue"></div>
                <input id="file_upload" name="file_upload" type="file">
            </div>

            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                <a onclick="doupload()" class="btn btn-default">Upload</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    var fileLanguage = ""
    $(function () {
        $('#file_upload').uploadifive({
            'auto': false,
            'fileSizeLimit': 50000,
            'multi': false,
            'height': 25,
            'queueID': 'queue',
            'uploadScript': '<g:createLink controller="upload" action="image"/>?format=json&fileType=image&module=gallery',
            'queueSizeLimit': 1,
            'onUploadComplete': function (file, data) {
                var jsonobj = $.parseJSON(data);
                if(jsonobj.status)
                {
                    $('#resultupload').html('Upload Complete');
                    upfilecomplete(jsonobj);
                }else{
                    $('#queue').html('');
                    $('#resultupload').html('Invalid File Type');
                    $('#file_upload').uploadifive('clearQueue');
                }
            }
        });
    });

    function upfilecomplete(data) {
        $('#imageName').val(data.filename);
        $('#previewthumb').html('<a class="thumbnail"><img src="' + data.pathfile + '"/></a>');
        $("#bModal").modal('hide');
    }

    function doupload() {
        $('#file_upload').uploadifive('upload')
    }

    function showDialog() {
        $('#resultupload').html('');
        $('#file_upload').uploadifive('clearQueue');
        $('#bModal').modal('show');
    }
    function previewimage(imagename)
    {
        var pathpreview = '<div class="thumbnail"><img src="'+imagename+'"/></div>';
        $('#previewimage').html(pathpreview);
        $('#pModal').modal('show');
    }
    $('#pModal').on('hidden', function () {
        $("#previewimage").html('');
    });
</script>