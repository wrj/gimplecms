<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Gallery : ${gallery.getTitleTh()}</title>
    <asset:stylesheet src="tabstyle.css"/>
    <ckeditor:resources/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/gallery">Gallery</a></li>
            <li><a href="<g:createLink action="editForm" id="${gallery.getId()}"/>">${gallery.getTitleTh()}</a></li>
            <li class="active">Gallery Image : Update</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="no-margin-top">Gallery : ${gallery.getTitleTh()}</h1>
        </div>
    </div>
    <g:form action="updateImage" method="POST" id="${gallery.getId()}" params="[iId:galleryImage.getId()]" class="form-horizontal">
        <g:render template="formImage" model="['gallery':gallery,'galleryImage':galleryImage]"/>
    </g:form>
</div>
</body>
</html>
