<div class="form-group">
    <label for="inputTitleEn" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10">
        <input type="text" name="titleEn" class="form-control" id="inputTitleEn" placeholder="title"
               value="${gallery.getTitleEn()}">
    </div>
</div>
<div class="form-group">
    <label for="inputDetailEn" class="col-sm-2 control-label">Detail</label>
    <div class="col-sm-10">
        <textarea name="detailEn" class="form-control"
                  id="inputDetailEn">${gallery.getDetailEn() ?: ""}</textarea>
    </div>
</div>