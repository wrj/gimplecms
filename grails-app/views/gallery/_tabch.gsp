<div class="form-group">
    <label for="inputTitleCh" class="col-sm-2 control-label">Title</label>

    <div class="col-sm-10">
        <input type="text" name="titleCh" class="form-control" id="inputTitleCh" placeholder="title"
               value="${gallery.getTitleCh()}" required>
    </div>
</div>

<div class="form-group">
    <label for="inputDetailCh" class="col-sm-2 control-label">Detail</label>

    <div class="col-sm-10">
        <textarea name="detailCh" class="form-control"
                  id="inputDetailCh">${gallery.getDetailCh() ?: ""}</textarea>
    </div>
</div>