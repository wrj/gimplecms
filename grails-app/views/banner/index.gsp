<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Banner</title>
    %{--<asset:stylesheet src="jquery.dataTables.css"/>--}%
    <asset:stylesheet src="dataTables.bootstrap.css"/>
    <asset:stylesheet src="ui-sortable.css"/>
    <asset:javascript src="jquery.dataTables.min.js"/>
    <asset:javascript src="dataTables.bootstrap.min.js"/>
    <asset:javascript src="jquery-ui.js"/>
</head>
<body>
    <g:if test="${flash.message}">
        <div class="alert alert-success" role="alert">
            ${flash.message}
        </div>
    </g:if>
    <div class="container-fluid">
        <div class="row">
            <ol class="breadcrumb">
                <li class="active">Front page banner</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h1 class="no-margin-top">Front page banner</h1>
            </div>
            <div class="col-md-6">
                <a href="<g:createLink action="newForm"/>" class="btn btn-success pull-right">Create</a>
                <a id="saveSort" class="btn btn-primary pull-right" style="margin-right: 10px;">
                    Save Sort
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul id="sortable" class="banner-item-list">
                    <g:each in="${bannerList}">
                        <li id="${it.getId()}" class="banner-image-${it.getId()}">
                            <div class="submenu">
                                <div class="col-md-3">
                                    <img src="<g:resource dir="uploadresource/banner/" file="${it.getImageTh()}"/>" class="img-thumb img-rounded col-md-12"/>
                                </div>
                                <div class="col-md-6"><span>${it.getTitleTh()}</span></div>
                                <div class="col-md-3">
                                    <a href='javascript:void(0);' class='btn btn-danger pull-right' onclick="alertDel(${it.getId()},'${it.getTitleTh()}')">Delete</a>
                                    %{--<a href='${createLink(action: "delete",id: it.getId())}' class='btn btn-warning pull-right'>Delete</a>--}%
                                    <a href='${createLink(action: "editForm",id: it.getId())}' class='btn btn-default pull-right' onclick="">Edit</a>
                                </div>
                            </div>
                        </li>
                    </g:each>
                </ul>
            </div>
        </div>
    </div>
<script>
    $( function() {
        $( "#sortable" ).sortable({
            placeholder: "ui-state-highlight"
        });
        $('#saveSort').click( function(e) {
            saveSort();
            return false;
        });
    } );

    function saveSort()
    {
        var sortList = []
        $("#sortable").find("li").each(function() {
            sortList.push($(this).attr("id"))
        })
        $.ajax({
            url: 'saveSort',
            method: "POST",
            data: { sortList : sortList.join(", ")},
            dataType: "json"
        }).done(function(result){
            if(result.status)
            {
                alert("Save Complete.")
            }
        })
    }
    
    function alertDel(id,message) {
        if (confirm('Are you sure you want to delete '+message+'?')) {
            $.ajax({
                url: '<g:createLink action="delete"/>/'+id,
                method: "POST",
                dataType: "json"
            }).done(function(result){
                if(result.status)
                {
                    alert("Delete Complete.")
                    $('.banner-image-'+id).remove()
                }
            })
        }
    }
</script>
</body>
</html>
