<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Banner : New</title>
    <asset:stylesheet src="tabstyle.css"/>
    <ckeditor:resources/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/banner">Frontpage Banner</a></li>
            <li class="active">Frontpage Banner : New</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="no-margin-top">Frontpage Banner : New</h1>
        </div>
    </div>
    <g:form action="create" method="POST" class="form-horizontal">
        <g:render template="form" model="['banner':banner]"/>
    </g:form>
</div>
</body>
</html>
