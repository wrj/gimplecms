<div class="form-group">
    <label for="inputTitleEn" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10">
        <input type="text" name="titleEn" class="form-control" id="inputTitleEn" placeholder="title"
               value="${page.getTitleEn()}">
    </div>
</div>

<div class="form-group">
    <label for="inputDetailEn" class="col-sm-2 control-label">Detail</label>

    <div class="col-sm-10">
        <ckeditor:editor name="detailEn" id="inputDetailEn" height="1000px" width="100%" toolbar="Mytoolbar">
            ${page.getDetailEn()?: ""}
        </ckeditor:editor>
    </div>
</div>
