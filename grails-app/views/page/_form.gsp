<g:if test="${flash.error}">
    <div class="alert alert-danger" role="alert">
        <g:eachError bean="${page}">
            <li>${it}</li>
        </g:eachError>
    </div>
</g:if>
<div class="row">
    <div class="form-group">
        <label for="inputAlias" class="col-sm-2 control-label">Alias</label>
        <div class="col-sm-10">
            <input type="text" name="alias" class="form-control" id="inputAlias" placeholder="alias" value="${page.getAlias()}" required>
        </div>
    </div>
    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#th" aria-controls="th" role="tab" data-toggle="tab">Thai</a></li>
            <li role="presentation"><a href="#en" aria-controls="en" role="tab" data-toggle="tab">English</a></li>
            <li role="presentation"><a href="#ch" aria-controls="ch" role="tab" data-toggle="tab">Chinese</a></li>
        </ul>
        <!-- Ck Custom Toolbar -->
        <ckeditor:config var="toolbar_Mytoolbar">
            [
                [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo'],[ 'Maximize'],
                [ 'Styles','Format'],[ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote'],
                [ 'Source', '-', 'Bold', 'Italic','Strike','-','RemoveFormat' ],
                [ 'Image','Table','HorizontalRule','SpecialChar'],['About']
            ]
        </ckeditor:config>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="th">
                <g:render template="tabth" model="[page:page]"/>
            </div>
            <div role="tabpanel" class="tab-pane" id="en">
                <g:render template="taben" model="[page:page]"/>
            </div>
            <div role="tabpanel" class="tab-pane" id="ch">
                <g:render template="tabch" model="[page:page]"/>
            </div>
        </div>

    </div>


    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="<g:createLink action="index"/>" class="btn btn-default">Cancel</a>
        </div>
    </div>
</div>

