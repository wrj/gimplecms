<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <asset:stylesheet src="bootstrap.css"/>
    <asset:stylesheet src="bootstrap-theme.css"/>
    <asset:stylesheet src="admin.css"/>
    <asset:stylesheet src="uploadifive.css"/>
    <asset:javascript src="jquery-2.2.0.min.js"/>
    <asset:javascript src="bootstrap.js"/>
    <asset:javascript src="jquery.uploadifive-v1.0.min.js"/>
    <g:layoutHead/>
</head>
<body>
    <g:render template="../layouts/navbar"/>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li <g:if test="${params.controller == 'register'}">class="active"</g:if>><a href="<g:createLink controller="register" action="index"/>">Registration<g:if test="${params.controller == 'register'}"><span class="sr-only">(current)</span></g:if></a></li>
                    <li <g:if test="${params.controller == 'banner'}">class="active"</g:if>><a href="<g:createLink controller="banner" action="index"/>">Front page banner<g:if test="${params.controller == 'banner'}"><span class="sr-only">(current)</span></g:if></a></li>
                    <li <g:if test="${params.controller == 'contact'}">class="active"</g:if>><a href="<g:createLink controller="contact" action="index"/>">Contact<g:if test="${params.controller == 'contact'}"><span class="sr-only">(current)</span></g:if></a></li>
                    <li <g:if test="${params.controller == 'page'}">class="active"</g:if>><a href="<g:createLink controller="page" action="index"/>">Page<g:if test="${params.controller == 'page'}"><span class="sr-only">(current)</span></g:if></a></li>
                    <li <g:if test="${params.controller == 'news'}">class="active"</g:if>><a href="<g:createLink controller="news" action="index"/>">News<g:if test="${params.controller == 'news'}"><span class="sr-only">(current)</span></g:if></a></li>
                    <li <g:if test="${params.controller == 'gallery'}">class="active"</g:if>><a href="<g:createLink controller="gallery" action="index"/>">Gallery<g:if test="${params.controller == 'gallery'}"><span class="sr-only">(current)</span></g:if></a></li>
                    <g:if test="${session.users.getUserLevel() > 0}">
                        <li <g:if test="${params.controller == 'users'}">class="active"</g:if>><a href="<g:createLink controller="users" action="index"/>">User<g:if test="${params.controller == 'users'}"><span class="sr-only">(current)</span></g:if></a></li>
                    </g:if>
                    <li><a href="<g:createLink controller="users" action="logout"/>">Logout</a></li>
                    <li><a href="<g:createLink controller="manualweb" action="index"/>" target="_blank">Help</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <g:layoutBody/>
            </div>
        </div>
    </div>
</body>
</html>