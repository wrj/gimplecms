<div class="form-group">
    <label for="inputTitleCh" class="col-sm-2 control-label">Title</label>

    <div class="col-sm-10">
        <input type="text" name="titleCh" class="form-control" id="inputTitleCh" placeholder="title"
               value="${news.getTitleCh()}" required>
    </div>
</div>

<div class="form-group">
    <label for="inputDescriptionCh" class="col-sm-2 control-label">Description</label>

    <div class="col-sm-10">
        <textarea name="descriptionCh" class="form-control"
                  id="inputDescriptionCh">${news.getDescriptionCh() ?: ""}</textarea>
    </div>
</div>

<div class="form-group">
    <label for="inputDetailCh" class="col-sm-2 control-label">Detail</label>

    <div class="col-sm-10">
        <ckeditor:editor name="detailCh" id="inputDetailCh" height="600px" width="100%" toolbar="Mytoolbar">
            ${news.getDetailCh() ?: ""}
        </ckeditor:editor>
    </div>
</div>
<!--- File Upload --->
<div class="form-group">
    <label for="imageCh" class="col-sm-2 control-label">File Upload</label>

    <div class="col-sm-10">
        <input type="text" value="" class="form-control" style="width:200px;float:left;margin-right:10px"
               readonly="readonly" name="imageCh" id="imageCh">
        <input type="button" value="Browse" onclick="showDialog('ch')" class="btn btn-default"/>
        <p class="help-block">ขนาดรูปภาพ 1533 * 1023 px</p>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Thumbnail</label>

    <div class="col-sm-10">
        <ul class="thumbnails">
            <g:if test="${news.getImageTh() && news.getImageTh() != ""}">
                <li class="col-md-3 li-imageCh">
                    <div class="thumbnail">
                        <a class="glyphicon glyphicon-trash pull-right-margin-right" href="javascript:deleteimage('imageCh')"></a>
                        <a class="glyphicon glyphicon-zoom-in pull-right" href="javascript:previewimage('<g:resource dir="uploadresource/news/" file="${news.getImageCh()}"/>')"></a>
                        <a href="javascript:previewimage('<g:resource dir="uploadresource/news/" file="${news.getImageCh()}"/>')">
                            <img src="<g:resource dir="uploadresource/news/" file="${news.getImageCh()}"/>" class="img-thumb img-rounded"/>
                        </a>
                        <p>Current Thumbnail</p>
                    </div>
                </li>
            </g:if>
            <li class="col-md-3" id="previewthumbch">

            </li>
        </ul>
    </div>
</div>