<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>News</title>
    %{--<asset:stylesheet src="jquery.dataTables.css"/>--}%
    <asset:stylesheet src="dataTables.bootstrap.css"/>
    <asset:javascript src="jquery.dataTables.min.js"/>
    <asset:javascript src="dataTables.bootstrap.min.js"/>
</head>
<body>
    <g:if test="${flash.message}">
        <div class="alert alert-success" role="alert">
            ${flash.message}
        </div>
    </g:if>
    <div class="container-fluid">
        <div class="row">
            <ol class="breadcrumb">
                <li class="active">News</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h1 class="no-margin-top">News</h1>
            </div>
            <div class="col-md-6">
                %{--<a href="<g:createLink action="index" params="[lang:'Th']"/>" class="btn btn-default">Th</a>--}%
                %{--<a href="<g:createLink action="index" params="[lang:'En']"/>" class="btn btn-default">En</a>--}%
                <a href="<g:createLink action="newForm"/>" class="btn btn-success pull-right">Create</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="example" class="table table-striped table-bordered display">
                    <thead>
                        <tr>
                            <th class="col-md-3">Title Th</th>
                            <th class="col-md-3">Title En</th>
                            <th class="col-md-3">Title Ch</th>
                            <th class="col-md-3">Tool</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
<script>
    var oTable = '';
    $(function () {
        oTable = $('#example').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "bJQueryUI": true,
            "fnDestroy": true,
            "bRetrieve": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": "<g:createLink action="list"/>"
        });
    })

    function alertDel(id,message) {
        if (confirm('Are you sure you want to delete '+message+'?')) {
            $.ajax({
                url: '<g:createLink action="delete"/>/'+id,
                method: "POST",
                dataType: "json"
            }).done(function(result){
                if(result.status)
                {
                    alert("Delete Complete.")
                    oTable.ajax.url( '<g:createLink action="list"/>' ).load();
                }
            })
        }
    }
</script>
</body>
</html>
