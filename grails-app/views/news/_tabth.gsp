<div class="form-group">
    <label for="inputTitleTh" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10">
        <input type="text" name="titleTh" class="form-control" id="inputTitleTh" placeholder="title" value="${news.getTitleTh()}" required>
    </div>
</div>
<div class="form-group">
    <label for="inputDescriptionTh" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-10">
        <textarea name="descriptionTh" class="form-control"
                  id="inputDescriptionTh">${news.getDescriptionTh() ?: ""}</textarea>
    </div>
</div>
<div class="form-group">
    <label for="inputDetailTh" class="col-sm-2 control-label">Detail</label>

    <div class="col-sm-10">
        <ckeditor:editor name="detailTh" id="inputDetailTh" height="600px" width="100%" toolbar="Mytoolbar">
            ${news.getDetailTh()?: ""}
        </ckeditor:editor>
    </div>
</div>
<!--- File Upload --->
<div class="form-group">
    <label for="imageTh" class="col-sm-2 control-label">File Upload</label>

    <div class="col-sm-10">
        <input type="text" value="" class="form-control" style="width:200px;float:left;margin-right:10px"
               readonly="readonly" name="imageTh" id="imageTh">
        <input type="button" value="Browse" onclick="showDialog('th')" class="btn btn-default"/>
        <p class="help-block">ขนาดรูปภาพ 1533 * 1023 px</p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Thumbnail</label>

    <div class="col-sm-10">
        <ul class="thumbnails">

            <g:if test="${news.getImageTh() && news.getImageTh() != ""}">
                <li class="col-md-3 li-imageTh">
                    <div class="thumbnail">
                        <a class="glyphicon glyphicon-trash pull-right-margin-right" href="javascript:deleteimage('imageTh')"></a>
                        <a class="glyphicon glyphicon-zoom-in pull-right-margin-right" href="javascript:previewimage('<g:resource dir="uploadresource/news/" file="${news.getImageTh()}"/>')"></a>
                        <a href="javascript:previewimage('<g:resource dir="uploadresource/news/" file="${news.getImageTh()}"/>')">
                            <img src="<g:resource dir="uploadresource/news/" file="${news.getImageTh()}"/>" class="img-thumb img-rounded"/>
                        </a>
                        <p>Current Thumbnail</p>
                    </div>
                </li>
            </g:if>

            <li class="col-md-3" id="previewthumbth">

            </li>
        </ul>
    </div>
</div>