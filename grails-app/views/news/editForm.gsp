<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>News : ${news.getTitleTh()}</title>
    <asset:stylesheet src="tabstyle.css"/>
    <ckeditor:resources/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/news">News</a></li>
            <li class="active">News : ${news.getTitleTh()}</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="no-margin-top">News : ${news.getTitleTh()}</h1>
        </div>
    </div>
    <g:form action="update" method="POST" id="${news.getId()}" class="form-horizontal">
        <g:render template="form" model="['news':news]"/>
    </g:form>
</div>
</body>
</html>
