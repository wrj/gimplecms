<%@ page contentType="text/html"%>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
    <table border="0" cellpadding="0" cellspacing="0" width="600px;" style="font-size:16px;" align="center" >
        <thead>
        <tr>
            <td colspan="2" style="background-color: #201E19; padding:10px 0px" align="center"><img src="http://www.melaniebangkok.com/assets/front/logo.png"></td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td colspan="2" style="border-bottom: 1px solid black; padding: 40px 0 10px 0; font-size: 12px;font-family: 'Tahoma';" align="center">
                We've got your inquiry. Thank you for your registration.
                <p style="color: #31099D;padding-bottom: 5px;margin-bottom: 0; font-size: 16px;font-family: 'Tahoma';">We will contact you via email or telephone as you submited.</p>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="color: #725103; padding: 37px 0 22px 0; font-weight: bold;font-family: 'Tahoma';" align="center">Your registration data</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid #979797;padding: 0px 0 5px 40px;color: #AB9767;font-family: 'Tahoma';">Title</td>
            <td style="border-bottom: 1px solid #979797;padding: 0px 0 5px 0px;color: #AB9767;font-family: 'Tahoma';">Information</td>
        </tr>
        <tr>
            <td style="color: #9B9B9B;padding: 20px 0 10px 40px;font-family: 'Tahoma';">First name</td>
            <td style="color: #4A4A4A;padding: 20px 0 10px 0;font-family: 'Tahoma';">${firstname}</td>
        </tr>
        <tr>
            <td style="color: #9B9B9B;padding: 10px 0 10px 40px;font-family: 'Tahoma';">Last name</td>
            <td style="color: #4A4A4A;padding: 10px 0 10px 0;font-family: 'Tahoma';">${lastname}</td>
        </tr>
        <tr>
            <td style="color: #9B9B9B;padding: 10px 0 10px 40px;font-family: 'Tahoma';">Mobile</td>
            <td style="color: #4A4A4A;padding: 10px 0 10px 0;font-family: 'Tahoma';">${mobile}</td>
        </tr>
        <tr>
            <td style="color: #9B9B9B;padding: 10px 0 10px 40px;font-family: 'Tahoma';">Email</td>
            <td style="color: #4A4A4A;padding: 10px 0 10px 0;font-family: 'Tahoma';">${email}</td>
        </tr>
        <tr>
            <td style="color: #9B9B9B;padding: 10px 0 10px 40px;font-family: 'Tahoma';">Price range</td>
            <td style="color: #4A4A4A;padding: 10px 0 10px 0;font-family: 'Tahoma';">${price}</td>
        </tr>
        <tr>
            <td style="color: #9B9B9B;padding: 10px 0 10px 40px;font-family: 'Tahoma';">Unit type</td>
            <td style="color: #4A4A4A;padding: 10px 0 10px 0;font-family: 'Tahoma';">${type}</td>
        </tr>
        <tr>
            <td style="color: #9B9B9B;padding: 10px 0 10px 40px;font-family: 'Tahoma';">Unit size</td>
            <td style="color: #4A4A4A;padding: 10px 0 10px 0;font-family: 'Tahoma';">${size}</td>
        </tr>
        <tr>
            <td style="color: #9B9B9B;padding: 10px 0 10px 40px;font-family: 'Tahoma';">Goal</td>
            <td style="color: #4A4A4A;padding: 10px 0 10px 0;font-family: 'Tahoma';">${goal}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid #979797;padding: 10px 0 20px 40px;color: #9B9B9B;font-family: 'Tahoma';">Show Case</td>
            <td style="border-bottom: 1px solid #979797;padding: 10px 0 20px 0;color: #4A4A4A;font-family: 'Tahoma';">${showCase}</td>
        </tr>
        </tbody>

        <tfoot>
            <tr>
                <td colspan="2" style="color:#D0021B; font-weight:bold; padding:25px 0 15px 0;font-family: 'Tahoma';" align="center">
                    <img style="width: 15px; padding-right: 5px" src="http://www.melaniebangkok.com/assets/front/icon-phone1.png"> Tel. 09-2925-2555
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="padding-bottom: 30px">
                    <img style="width: 60px" src="http://www.melaniebangkok.com/assets/front/icon-social1.png">
                </td>
            </tr>
            <tr>
                <td style="font-size: 10px; color: #AB9767; padding: 0 0px 70px 40px; vertical-align: top;" align="left">
                    <b>Sale Gallery</b><br/>
                    <span style="font-weight: 200;font-family: 'Tahoma';">Melanie Bangkok Project<br/>
                        No.8 Soi Chan 9, Chan Road, Chongnonsi, Yannawa, Bangkok 10120, Thailand</span>
                </td>
                <td style="font-size: 10px; color: #AB9767; padding: 0 40px 70px 40px; vertical-align: top;" align="left">
                    <b>Head Office</b><br/>
                    <span style="font-weight: 200;font-family: 'Tahoma';">Primegate Realty Co.,Ltd.<br/>
                        31,33,35 Rama 2-52 Sa-Maedam, Bangkhuntian, Bangkok 10150, Thailand</span>
                </td>
            </tr>
        </tfoot>
    </table>
</body>
</html>