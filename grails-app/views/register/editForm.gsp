<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Registration : ${register.getFirstName()} ${register.getLastName()}</title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/users">Registration</a></li>
            <li class="active">Registration : ${register.getFirstName()} ${register.getLastName()}</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="no-margin-top">Registration : ${register.getFirstName()} ${register.getLastName()}</h1>
        </div>
    </div>
    <g:form action="update" method="POST" id="${register.getId()}" class="form-horizontal">
        <g:render template="form" model="['register':register,'priceRange':priceRange,'unitType':unitType,'unitSize':unitSize]"/>
    </g:form>
</div>
</body>
</html>
