<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
	<meta name="twitter:widgets:theme" content="light">
	<meta property="og:title" content="Melanie Bangkok ถนนจันทร์-สาทร" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="http://www.melaniebangkok.com/images/slide3.jpg" />
	<meta property="og:description" content="Melanie Bangkok ถนนจันทร์-สาทร" />
	<title>Melanie Bangkok ถนนจันทร์-สาทร</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/theme-mountain-favicon.ico">

	<!-- Font -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700%7CLato:300,400,700' rel='stylesheet' type='text/css'>
	
	<!-- Css -->
	<link rel="stylesheet" href="css/core.min.css" />
	<link rel="stylesheet" href="css/skin-architecture.css" />
    <link rel="stylesheet" href="css/them.css" />
    <!-- bxSlider CSS file -->
<link href="jquery.bxslider/jquery.bxslider.css" rel="stylesheet" />
	<!--[if lt IE 9]>
    	<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<style>
.bx-wrapper .bx-viewport{
    border: none;
    background: none;
}
.row{
    max-width: none;
    width: 100%;
}
.column{
    padding: 0;
    
}
</style>
</head>
<body class="shop home-page" id="top">

	<!-- Side Navigation Menu -->
	<aside class="side-navigation-wrapper enter-right" data-no-scrollbar data-animation="scale-in">
		<div class="side-navigation-scroll-pane">
			<div class="side-navigation-inner">
				<div class="side-navigation-header">
					<div class="navigation-hide side-nav-hide">
						<a href="#">
							<span class="icon-cancel medium"></span>
						</a>
					</div>
				</div>
				<nav class="side-navigation">
					<ul>
                    <li><a href="index.php">HOME</a></li>
                                    <li><a href="project.php" >PROJECT</a></li>
                                    <li><a href="unit.php">UNIT PLAN</a></li>
                                    <li><a href="index.php#gallery" >GALLERY</a></li>
                                    <li><a href="index.php#news">NEWS & PROGRESS</a></li>
                                    <li><a href="contact.php" >CONTACT</a></li>
                                    <li><a href="developer.php">DEVELOPER</a></li>
					</ul>
				</nav>
				<div class="side-navigation-footer">
					<ul class="social-list list-horizontal">
						<li><a href="#"><span class="icon-twitter small"></span></a></li>
						<li><a href="#"><span class="icon-facebook small"></span></a></li>
						<li><a href="#"><span class="icon-instagram small"></span></a></li>
					</ul>
					<p class="copyright no-margin-bottom">&copy; 2014 THEMEMOUNTAIN.</p>
				</div>
			</div>
		</div>
	</aside>
	<!-- Side Navigation Menu End -->

	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">

			<!-- Header -->
			<header class="header header-fixed  header-fixed-on-mobile header-transparent header-background header-sticky " data-sticky-threshold="200" data-bkg-threshold="100">
				<div class="header-inner">
					<div class="row nav-bar">
						<div class="column width-12 nav-bar-inner">
							<div class="logo">
								<div class="logo-inner">
									<a href="index.php"><img src="images/logo.png" alt="melanine Logo" /></a>
									<a href="index.php"><img src="images/logo.png" alt="melanine Logo" /></a>
								</div>
							</div>
							<nav class="navigation nav-block secondary-navigation nav-right">
								<ul>
									<!-- Dropdown Cart Overview
									<li>
										<div class="dropdown">
											<a href="#" class="nav-icon cart button no-page-fade"><span class="cart-indication"><span class="icon-shopping-cart"></span> <span class="badge">3</span></span></a>
											<ul class="dropdown-list custom-content cart-overview">
												<li class="cart-item">
													<a href="single-product.html" class="product-thumbnail">
														<img src="images/design-agency/portfolio/grid/no-margins/project-6-square.jpg" alt="" />
													</a>
													<div class="product-details">
														<a href="single-product.html" class="product-title">
															Cotton Jump Suit
														</a>
														<span class="product-quantity">2 x</span>
														<span class="product-price"><span class="currency">$</span>15.00</span>
														<a href="#" class="product-remove icon-cancel"></a>
													</div>
												</li>
												<li class="cart-item">
													<a href="single-product.html" class="product-thumbnail">
														<img src="images/design-agency/portfolio/grid/no-margins/project-7-square.jpg" alt="" />
													</a>
													<div class="product-details">
														<a href="single-product.html" class="product-title">
															Cotton Jump Suit
														</a>
														<span class="product-quantity">2 x</span>
														<span class="product-price"><span class="currency">$</span>15.00</span>
														<a href="#" class="product-remove icon-cancel"></a>
													</div>
												</li>
												<li class="cart-item">
													<a href="single-product.html" class="product-thumbnail">
														<img src="images/design-agency/portfolio/grid/no-margins/project-8-square.jpg" alt="" />
													</a>
													<div class="product-details">
														<a href="single-product.html" class="product-title">
															Cotton Jump Suit
														</a>
														<span class="product-quantity">2 x</span>
														<span class="product-price"><span class="currency">$</span>15.00</span>
														<a href="#" class="product-remove icon-cancel"></a>
													</div>
												</li>
												<li class="cart-subtotal">
													Sub Total
													<span class="amount"><span class="currency">$</span>15.00</span>
												</li>
												<li class="cart-actions">
													<a href="cart.html" class="view-cart">View Cart</a>
													<a href="checkout.html" class="checkout button small"><span class="icon-check"></span> Checkout</a>
												</li>
											</ul>
										</div>
									</li> -->
									<!-- Dropdown Search Module
									<li>
										<div class="dropdown">
											<a href="#" class="nav-icon search button no-page-fade"><span class="icon-magnifying-glass"></span></a>
											<div class="dropdown-list custom-content cart-overview">
												<div class="search-form-container site-search">
													<form action="#" method="get" novalidate>
														<div class="row">
															<div class="column width-12">
																<div class="field-wrapper">
																	<input type="text" name="search" class="form-search form-element no-margin-bottom" placeholder="type &amp; hit enter...">
																	<span class="border"></span>
																</div>
															</div>
														</div>
													</form>
													<div class="form-response"></div>
												</div>
											</div>
										</div>
									</li> -->
									<li class="aux-navigation hide">
										<!-- Aux Navigation -->
										<a href="#" class="navigation-show side-nav-show nav-icon">
											<span class="icon-menu"></span>
										</a>
									</li>
								</ul>
							</nav>
							<nav class="navigation nav-block primary-navigation nav-right no-margin-right">
								<ul>
                                    <li><a href="index.php">HOME</a></li>
                                    <li><a href="project.php" >PROJECT</a></li>
                                    <li><a href="unit.php" >UNIT PLAN</a></li>
                                    <li><a href="index.php#gallery" >GALLERY</a></li>
                                    <li><a href="index.php#news" >NEWS & PROGRESS</a></li>
                                    <li><a href="contact.php" >CONTACT</a></li>
                                    <li><a href="developer.php">DEVELOPER</a></li>
                                    
                                </ul>
							</nav>
						</div>
					</div>
				</div>
			</header>
			<!-- Header End -->

			<!-- Content -->
			<div class="content clearfix" >

			

			<!-- Hero Section 5 -->
                <section id="unit">
                <div  style="position: relative;width: 100%;height: 100%;float: left;">
                <img  src="images/planimg.jpg" />
               <div style="position: absolute;bottom: 0;width: 100%;z-index: 99;"> <div class="project-menu">
                    <p>Melanie Bangkok สถาปัตยกรรมสไตล์ยูโรเปียนคลาสสิกที่ผสมผสานความเป็นอยู่ที่อบอุ่นแนวตะวันออก 
ยูนิตเน้นการดีไซน์ให้มีความโปร่งโล่ง มี Bay Window เปิดรับแสงแดดธรรมชาติ ช่วยเพิ่มพืนที่ใช้สอยภายใน 
เลือกสรรวัสดุเทียบเคียงมาตรฐานสากล สะท้อนรสนิยมหรูของผู้พักอาศัย</p>
<ul>
    <li><a href="javascript:;" id="slide-to1">ONE BEDROOM</a></li>
    <li><a href="javascript:;" id="slide-to2">TWO BEDROOM</a></li>
    
</ul>
                </div></div></div>
                	<div class="section-block plan  pt-0 pb-0 full-width no-margins no-padding ">
  <ul class="bxslider">
      <li><div class="row">
						<div class="column width-6 " >
							<div class="hero-content split-hero-content">
								<div style="padding: 40px;" class="hero-content-inner center horizon" data-animate-in="preset:slideInRightShort;duration:1000ms;" data-threshold="0.5">
									<h2 class="mb-30" style="color: #ab9767 !important;">ONE BEDROOM</h2>
                                    <hr style="border:2px solid #ab9767;" class="mb-30"  />
									<table class="table" style="border: none;width: 250px;"  align="center">
	<tbody >
		<tr onclick="changPlan('images/a1.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE A1   </td>
			<td>27 SQ.M.</td>
		</tr>
		<tr onclick="changPlan('images/a2.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE A2   </td>
			<td>35 SQ.M.</td>
		</tr>
        <tr onclick="changPlan('images/a3.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE A3   </td>
			<td>35 SQ.M.</td>
		</tr>
		<tr onclick="changPlan('images/a3-1.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE A3-1   </td>
			<td>35 SQ.M.</td>
		</tr>
        <tr onclick="changPlan('images/a3-2.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE A3-2   </td>
			<td>35 SQ.M.</td>
		</tr>
        <tr onclick="changPlan('images/plan_A3-3.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE A3-3   </td>
			<td>35 SQ.M.</td>
		</tr>
        <tr onclick="changPlan('images/plan_A4.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE A4   </td>
			<td>35 SQ.M.</td>
		</tr>
        <tr onclick="changPlan('images/plan_A4.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE A4-1   </td>
			<td>40 SQ.M.</td>
		</tr>
        <tr onclick="changPlan('images/plan_A5.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE A5   
    </td>
			<td>43 SQ.M.</td>
		</tr>
        <tr onclick="changPlan('images/plan_A5-1.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE A5-1    </td>
			<td>48 SQ.M.</td>
		</tr>
        <tr onclick="changPlan('images/plan_A6.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE A6   
    </td>
			<td>43 SQ.M.</td>
		</tr>
        <tr onclick="changPlan('images/plan_A6-1.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE A6-1    </td>
			<td>49 SQ.M.</td>
		</tr>
	</tbody>
</table>
								</div>
							</div>
						</div>
                        <div class="column width-6"><img id="oneb" src="images/a1.jpg"  alt=""/></div>
					</div></li>
      <li><div class="row">
						<div class="column width-6 " >
							<div class="hero-content split-hero-content">
								<div style="padding: 40px;" class="hero-content-inner center horizon" data-animate-in="preset:slideInRightShort;duration:1000ms;" data-threshold="0.5">
									<h2 class="mb-30" style="color: #ab9767 !important;">TWO BEDROOM</h2>
                                     <hr style="border:2px solid #ab9767;" class="mb-30"  />
											<table class="table " style="border: none;width: 250px;"  align="center">
	<tbody >
		<tr onclick="changPlan2('images/plan_B1.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE B1   </td>
			<td>56 SQ.M.</td>
		</tr>
		<tr onclick="changPlan2('images/plan_B2.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE B2   </td>
			<td>61 SQ.M.</td>
		</tr>
        <tr onclick="changPlan2('images/plan_B3.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE B3   </td>
			<td>61 SQ.M.</td>
		</tr>
        <tr onclick="changPlan2('images/plan_B4.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE B4   </td>
			<td>68 SQ.M.</td>
		</tr>
		<tr onclick="changPlan2('images/plan_B4-1.jpg')" style="cursor: pointer;">
			<td style="color: #ab9767;font-size: 18px;">TYPE B4-1   </td>
			<td>77 SQ.M.</td>
		</tr>
	</tbody>
</table>
								</div>
							</div>
						</div>
                        <div class="column width-6"><img id="oneb2" src="images/plan_B1.jpg"  alt=""/></div>
					</div>
							</li>
  </ul>
                	<!-- Hero Slider -->
				                       
				<!-- Hero Section 5 End -->
                </section>
				



			</div>
			<!-- Content End -->

			<!-- Footer -->
			<footer style="background:#f5f5f5;font-family: rsulight;font-size: 18px;color:#ac9766;">
	<div class="container center">
   
    CALL 092 925 2555
    <a style="color:#ac9766;" href="mailto:info@primegatesrealty.com"><i class="icon-mail" aria-hidden="true"></i><span style="font-family: sans-serif;font-size: 14px;"> Email Us</span></a>
    <a target="_blank" href="https://www.facebook.com/melaniebangkok/" style="color:#ac9766;"><i class="icon-facebook" aria-hidden="true"></i></a>
    <a target="_blank" href="https://www.instagram.com/melanie_bangkok/" style="color:#ac9766;"><i class="icon-instagram" aria-hidden="true"></i></a>


     <img src="../images/images/footer_05.jpg" />
    <img src="../images/images/footer_06.jpg" /> <img src="../images/images/footer_07.jpg" />
    </div>
  
</footer>
			<!-- Footer End -->

		</div>
	</div>

	<!-- Js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maps.googleapis.com/maps/api/js?v=3"></script>
	<script src="js/timber.master.min.js"></script>
    <script src="/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript">
    function changPlan(src){
        
        $('#oneb') .fadeOut(400, function() {$('#oneb') .attr("src", src)}).fadeIn(400);
    }
      function changPlan2(src){
        console.log(src);
        $('#oneb2') .fadeOut(400, function() {$('#oneb') .attr("src", src)}).fadeIn(400);
    }
		$( document ).ready( function() {
				
		var slider = $('.bxslider').bxSlider({
        controls:false
    
    
   });

        
        
        
        
             $( '#slide-to2' ).on( 'click', function(){
           
				slider.goToSlide(1);
                
			});
             $( '#slide-to1' ).on( 'click', function(){
           
				slider.goToSlide(0);
                
			});
          
            	});
</script>
        
</body>
</html>