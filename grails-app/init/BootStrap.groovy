import melaniebangkokbackoffice.Users

class BootStrap {

    def init = { servletContext ->
        if(Users.count() == 0)
        {
            Users u = new Users()
            u.setFirstName("Melanie")
            u.setLastName("Bangkok")
            u.setEmail("saadmin@melaniebangkok.com")
            u.setUsername("saadmin")
            u.setPassword("password")
            u.setUserLevel(2)
            u.save(flush:true)
        }
    }
    def destroy = {
    }
}
