package melaniebangkokbackoffice

class Contact {

    String name
    String email
    String phone
    String message
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }

    static mapping = {
        id generator:'sequence',params : [sequence:'sequence_contact_id']
    }
}
