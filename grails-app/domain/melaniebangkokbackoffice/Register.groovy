package melaniebangkokbackoffice

class Register {

    String firstName
    String lastName
    String email
    String tel
    Integer priceRange
    Integer unitType
    Integer unitSize
    Integer goal
    Boolean showCase
    Date dateCreated
    Date lastUpdated

    static constraints = {
        showCase nullable: true
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'sequence_register_id']
    }
}
