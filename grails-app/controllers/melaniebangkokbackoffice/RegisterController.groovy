package melaniebangkokbackoffice

import grails.converters.JSON

class RegisterController {

    def index() {

    }

    def editForm() {
        Register register = Register.get(params.id)


        def priceRange = [:]
        priceRange[1] = "Below 4 Mil.Baht"
        priceRange[2] = "4-5 Mil.Baht"
        priceRange[3] = "5.1-6 Mil.Baht"
        priceRange[4] = "6.1-7 Mil.Baht"
        priceRange[5] = "7.1-8 Mil.Baht"
        priceRange[6] = "8.1-9 Mil.Baht"
        priceRange[7] = "9.1-10 Mil.Baht"
        priceRange[8] = "10 Mil.Baht"

        def unitType = [:]
        unitType[1] = "1 Bed"
        unitType[2] = "2 Bed"

        def unitSize = [:]
        unitSize[1] = "Below 35"
        unitSize[2] = "35-40"
        unitSize[3] = "41-50"
        unitSize[4] = "51-60"
        unitSize[5] = "61-70"
        unitSize[6] = "71-80"
        unitSize[7] = "81-90"
        unitSize[8] = "91-100"
        unitSize[9] = "Above 100"

        def goal = [:]
        goal[1] = "จุดประสงค์ในการซื้อ"
        goal[2] = "บ้านหลังแรก"
        goal[3] = "บ้านหลังที่สอง"
        goal[4] = "เพื่อลงทุน (ขายต่อ)"
        goal[5] = "เพื่อลงทุน (ปล่อยเช่า)"
        goal[6] = "เพื่ออยู่อาศัยเอง"
        goal[7] = "ใกล้ที่ทำงาน"


        [register: register,priceRange:priceRange,unitType:unitType,unitSize:unitSize,goal:goal]


    }


    def update() {
        Register register = Register.get(params.id)
        register.properties = params
        if (!register.save())
        {
            flash.error = "Have Error."
            render(view: "editForm",id:params.id, model: [register: register])
        }else{
            flash.message = "Update Complete."
            redirect(action: "index")
        }
    }

    def delete() {
        def result = [status:true]
        Register register = Register.get(params.id)
        if (!register)
        {
            result["message"] = "Have Error."
            result["status"] = false
        }else{
            register.delete()
            result["message"] = "Delete Complete."
        }
        render result as JSON
    }

    def list() {
        String sortlabel = "firstName"
        Integer sortvalue = 1
        String searchtxt = ""
        List listColumns = ["firstName","lastName","email","tel"]
        if(!params.containsKey('iSortCol_0'))
        {
            params.iSortCol_0 = 0
            params.sSortDir_0 = "asc"
            params.sSearch = ""
            params.sEcho = 1
            params.iDisplayStart =0
            params.iDisplayLength = 10
        }
        if(params["iSortCol_0"].toInteger() <= (listColumns.size() - 2))
        {
            sortlabel = listColumns.getAt(params["iSortCol_0"].toInteger())
        }
        def registerList = Register.findAllByFirstNameIlikeOrLastNameIlikeOrEmailIlikeOrTelIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%",[max: params["iDisplayLength"].toInteger(),sort: sortlabel,order: params["sSortDir_0"],offset: params["iDisplayStart"].toInteger()])
        def registerTotal = Register.countByFirstNameIlikeOrLastNameIlikeOrEmailIlikeOrTelIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%")
        def sourcedatatable = [sEcho:params["sEcho"].toInteger()]
        sourcedatatable["iTotalRecords"] = registerTotal
        sourcedatatable["iTotalDisplayRecords"] = registerTotal
        List nData = []
        registerList.each {
            def priceRange = ""
            if(it.getPriceRange() == 1)
            {
                priceRange = "Below 4 Mil.Baht"
            }
            if(it.getPriceRange() == 2)
            {
                priceRange = "4-5 Mil.Baht"
            }
            if(it.getPriceRange() == 3)
            {
                priceRange = "5.1-6 Mil.Baht"
            }
            if(it.getPriceRange() == 4)
            {
                priceRange = "6.1-7 Mil.Baht"
            }
            if(it.getPriceRange() == 5)
            {
                priceRange = "7.1-8 Mil.Baht"
            }
            if(it.getPriceRange() == 6)
            {
                priceRange = "8.1-9 Mil.Baht"
            }
            if(it.getPriceRange() == 7)
            {
                priceRange = "9.1-10 Mil.Baht"
            }
            if(it.getPriceRange() == 8)
            {
                priceRange = "10 Mil. Baht"
            }

            def unitType = ""
            if(it.getUnitType() == 1)
            {
                unitType = "1 Bed"
            }
            if(it.getUnitType() == 2)
            {
                unitType = "2 Bed"
            }

            def unitSize = ""
            if(it.getUnitSize() == 1)
            {
                unitSize = "Below 35"
            }
            if(it.getUnitSize() == 2)
            {
                unitSize = "35-40"
            }
            if(it.getUnitSize() == 3)
            {
                unitSize = "41-50"
            }
            if(it.getUnitSize() == 4)
            {
                unitSize = "51-60"
            }
            if(it.getUnitSize() == 5)
            {
                unitSize = "61-70"
            }
            if(it.getUnitSize() == 6)
            {
                unitSize = "71-80"
            }
            if(it.getUnitSize() == 7)
            {
                unitSize = "81-90"
            }
            if(it.getUnitSize() == 8)
            {
                unitSize = "91-100"
            }
            if(it.getUnitSize() == 9)
            {
                unitSize = "Above 100"
            }


            List iData = [it.getFirstName(),it.getLastName(),it.getEmail(),it.getTel(),priceRange,unitType,unitSize,"<a href='${createLink(action: "editForm",id: it.getId())}' class='btn btn-default'>View</a> <a href='javascript:void(0);' onclick=\"alertDel(${it.getId()},'${it.getFirstName()}')\" class='btn btn-danger'>Delete</a>"]
            nData.push(iData)
        }
        sourcedatatable["aaData"] = nData
        render sourcedatatable as JSON
    }
}
