package melaniebangkokbackoffice

import grails.util.Holders

class ManualwebController {

    def index() {
        def grailsApplication = Holders.getGrailsApplication()
        def htmlContent = new File("${grailsApplication.config.pathProject}grails-app/assets/manual/index.txt").text
        [htmlContent:htmlContent]
    }
}
