package melaniebangkokbackoffice

import grails.converters.JSON

class PageController {

    def uploadService

    def index() {
    }

    def newForm() {
        Page page = new Page()
        [page:page]
    }

    def editForm() {
        Page page = Page.get(params.id)
        [page:page]
    }

    def create() {
        Page page = new Page(params)
        if (!page.save()) {
            flash.error = "Have Error"
            render(view: "newForm", model: [page: page])
        }

        else{
        flash.message = "Create Complete."
        redirect(action: "index")
        }
    }

    def update() {
        Page page = Page.get(params.id)
        page.properties = params
        if (!page.save()) {
            flash.errors = "Have Error."
            render(view: "editForm",id:params.id, model: [page:page])
        }
        else {
            flash.message = "Update Complete"
            redirect(action:'index')
        }
    }

    def delete() {
        def result = [status:true]
        Page page = Page.get(params.id)
        if (!page)
        {
            result["message"] = "Have Error."
            result["status"] = false
        }else{
            page.delete()
            result["message"] = "Delete Complete."
        }
        render result as JSON
    }

    def list() {
        String sortlabel = "titleTh"
        Integer sortvalue = 1
        String searchtxt = ""
        List listColumns = ["titleTh","titleEn","titleCh"]
        if(!params.containsKey('iSortCol_0'))
        {
            params.iSortCol_0 = 0
            params.sSortDir_0 = "asc"
            params.sSearch = ""
            params.sEcho = 1
            params.iDisplayStart =0
            params.iDisplayLength = 10
        }
        if(params["iSortCol_0"].toInteger() <= (listColumns.size() - 2))
        {
            sortlabel = listColumns.getAt(params["iSortCol_0"].toInteger())
        }
        def pageList = Page.findAllByAliasIlikeOrTitleThIlikeOrTitleEnIlikeOrTitleChIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%",[max: params["iDisplayLength"].toInteger(), sort: sortlabel, order: params["sSortDir_0"], offset: params["iDisplayStart"].toInteger()])
        def pageTotal = Page.countByAliasIlikeOrTitleThIlikeOrTitleEnIlikeOrTitleChIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%")
        def sourcedatatable = [sEcho:params["sEcho"].toInteger()]
        sourcedatatable["iTotalRecords"] = pageTotal
        sourcedatatable["iTotalDisplayRecords"] = pageTotal
        List nData = []
        pageList.each {
            List iData = [it.getTitleTh(),it.getTitleEn(),it.getTitleCh(),"<a href='${createLink(action: "editForm",id: it.getId())}' class='btn btn-default'>Edit</a> <a href='javascript:void(0);' onclick=\"alertDel(${it.getId()},'${it.getAlias()}')\" class='btn btn-danger'>Delete</a>"]
            nData.push(iData)
        }
        sourcedatatable["aaData"] = nData
        render sourcedatatable as JSON
    }
}
