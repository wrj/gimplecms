package melaniebangkokbackoffice

import grails.converters.JSON

class ContactController {

    def index() {

    }

    def editForm() {
        Contact contact = Contact.get(params.id)
        [contact:contact]
    }

    def update() {
        Contact contact = Contact.get(params)
        contact.properties = params
        if (!contact.save()) {
            flash.errors = "Have Error."
            render(view: "editForm",id:params.id, model: [contact:contact])
        }
        else {
            flash.message = "Update Complete"
            redirect(action:'index')
        }
    }

    def delete() {
        def result = [status:true]
        Contact contact = Contact.get(params.id)
        if (!contact)
        {
            result["message"] = "Have Error."
            result["status"] = false
        }else{
            contact.delete()
            result["message"] = "Delete Complete."
        }
        render result as JSON
    }

    def list() {
        String sortlabel = "name"
        Integer sortvalue = 1
        String searchtxt = ""
        List listColumns = ["name","email","phone","message"]
        if(!params.containsKey('iSortCol_0'))
        {
            params.iSortCol_0 = 0
            params.sSortDir_0 = "asc"
            params.sSearch = ""
            params.sEcho = 1
            params.iDisplayStart =0
            params.iDisplayLength = 10
        }
        if(params["iSortCol_0"].toInteger() <= (listColumns.size() - 2))
        {
            sortlabel = listColumns.getAt(params["iSortCol_0"].toInteger())
        }
        def contactList = Contact.findAllByNameIlikeOrEmailIlikeOrPhoneIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%",[max: params["iDisplayLength"].toInteger(),sort: sortlabel,order: params["sSortDir_0"],offset: params["iDisplayStart"].toInteger()])
        def contactTotal = Contact.countByNameIlikeOrEmailIlikeOrPhoneIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%")
        def sourcedatatable = [sEcho:params["sEcho"].toInteger()]
        sourcedatatable["iTotalRecords"] = contactTotal
        sourcedatatable["iTotalDisplayRecords"] = contactTotal
        List nData = []
        contactList.each {
            List iData = [it.getName(),it.getEmail(),it.getPhone(),it.getMessage(),"<a href='${createLink(action: "editForm",id: it.getId())}' class='btn btn-default'>View</a> <a href='javascript:void(0);' onclick=\"alertDel(${it.getId()},'${it.getName()}')\" class='btn btn-danger'>Delete</a>"]
            nData.push(iData)
        }
        sourcedatatable["aaData"] = nData
        render sourcedatatable as JSON
    }

}
